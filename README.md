**Description**

Midibop is a simple to use C++ Midi input library. Midibop manages midi
devices and allows the developer to easily query the current state of
the midi input device, in terms of Notes and Octaves, rather than Codes
and Signals.

Midibop is a wrapper around the excellent, lower level RtMidi Library.
You can find it [here](https://github.com/thestk/rtmidi)
<br></br>
<br></br>
**Installation**

Midibop comes as a two header files and a .cpp file. Extract the archive
from download, and place MidiDefinitions.h, MidiDevice.h, and
MidiDevice.cpp in your project.

You will also need the RtMidi library, this is also provided in the
download. You can copy and paste the RtMidi folder into you project, and
add the four files contained within as usual.

All of these files are contained within the MidiBopLib directory of the download archive.
<br></br>
<br></br>
**Usage**


***Setup***

First, create a MidiDevice object and initialise it. I recommend creating the
object using a smart pointer.

    std::unique_ptr<MidiDevice> midiDevice = std::unique_ptr<MidiDevice>();
    midiDevice->Initialise();

Assuming a midi device is connected, the Initialise method should setup
a connection automatically.

In order for the midi device to track its own state, the Update() method
of your newly created MidiDevice object must be called in your
applications main update loop, wherever that may be.

    void Update()
    {
        midiDevice->Update();
    }

Your midi device should now be connected and tracking its own state. If
Initialise() was called before the midi device was connected to your PC
or before it was turned on, by default, Update() will attempt to connect
to the first available device when it becomes available. You can disable
this behaviour with DisableAutomaticConnection() and instead call
Initialise() manually to establish connections.

<br></br>
<br></br>
***Querying Midi State***

**Getting Whether Keys Are Pressed**

Having setup a self-tracking midi device, It is simple to query whether
a key is currently pressed.

    //Checks if the C key in the 5th octave is currently pressed
    
    bool isCKeyPressed = midiDevice->KeyPressed(MidiBop::Note::C, 5);

One can also query the device via midi-code rather than note and octave.

    //Checks if the Middle-C key is currently pressed as represented by Midi-Code 60
    
    bool isCKeyPressed = midiDevice->KeyPressed(60);

In addition, one can also check if a collection of notes, or a chord, is
pressed, by passing in a vector of midi-codes to the KeysPressed()
method

    bool isCTriadPressed = midiDevice->KeysPressed(std::vector<int>{60,64,67});

<br></br>
<br></br>
**Pressure**

A lot of midi devices, especially midi keyboards, also register pressure
values. You can query the current pressure values of a specific note
similar to how you can get whether a note is currently pressed.

    //Returns the pressure of the C key in the 5th octave.
    
    int notePressure = midiDevice->GetKeyPressure(MidiBop::Note::C, 5);

    //Returns the key pressure of middle C, on midiCode 60.
    
    int notePressure = midiDevice->GetKeyPressure (60);

If the note queried is currently not pressed, GetKeyPressure() will
return 0;

<br></br>
<br></br>
**Full State**

If you wish, you can also grab a map of the entire device state at any
time, which is a map of midicodes representing keys, linked to a pair of
pressure ints and timestamp doubles of the last recorded event about the
note in question. The function to grab that map is defines thusly, and
like all the other functions thus far, can be accessed through your
created midiDevice object.

    ///Get a map containing the current state of the entire midi-board. Key
    is the midicode, value is a pair of the intensity of push, and the
    processor timestamp the last event about this key was registered.
    Intensities of 0 mean unpushed.
    
    std::map<int, std::pair<int, double>> GetKeys() const;

For convenience, you can also access a filtered version of this
function, which will return a map containing only keys that are
currently pressed.

    ///Get a map containing only the keys that are currently down. Key of
    the map is the midicode, value is a pair of intensity of push, and the
    processor timestamp of the last event about this key was registered.
    
    std::map<int, std::pair<int, double>> GetPressedKeys() const;

<br></br>
<br></br>
***Callbacks***

Sometimes you want to react to an event happening on your midi-device,
usually a key being pressed/released. To facilitate this, Midibop
facilitates registering function callbacks via the use of std::function.

Midibop provides callbacks for Key Pressed, Key Released, Key Intensity
Changed, Device Connected, and Device Disconnected events.

To give an example, most applications will want to do something like
register a function OnKeyPressed to be called whenever a key on your
device has been pressed. The function below used the arguments passed to
detect if the key pressed is any C note and then output the intensity of
the press.

    void OnKeyPressed (int midiCode, int pressIntensity)
    {
        if (MidiBop::GetNoteFromIntCode(midiCode) == MidiBop::Note::C)
        {
            std::cout << "C Intensity : " + std::to_string(pressIntensity);
        }
    }
    
    midiDevice->RegisterKeyPressedCallback (OnKeyPressed);

OnKeyPressed will now we called whenever a key (or your device
equivalent) is pressed, and the parameters passed in will be of the
appropriate midiCode and pressIntensity.

If you don’t want to check what key is pressed, but just register a
specific function to be called whenever say, the C key in the 6^th^
octave is pressed, you can do :

    void OnCKeyInSixthOctavePressed (int midiCode, int pressIntensity)
    {
        std::cout << "C Intensity : " + std::to_string(pressIntensity);
    }

    midiDevice->RegisterNoteInOctavePushedCallback (MidiBop::Note::C, 6, OnCKeyInSixthOctavePressed);

One can also deregister callbacks down the line with :

    midiDevice->DeRegisterKeyPressedCallback(OnKeyPressed);

After this line has executed, OnKeyPressed will no longer be called when
a key is pressed.

Or rather just deregister all key pressed callbacks with

    midiDevice->DeregisterAllKeyPressedCallbacks();

Or even just deregister all callbacks of all types.

    midiDevice->DeregisterAllCallbacks();

<br></br>
<br></br>
***Utility***

Midibop also comes with a few utility functions defined in
MidiDefinitions.h. These are mainly for converting nicely between
midiCodes and useable Notes. Assuming you have access to the MidiBop
namespace, you can call any of these functions statically via
MidiBop::Function()

For convenience, these functions are listed below.

    ///Returns what note an rtMidi int code represents
    static const Note GetNoteFromIntCode(const int code)

    ///Returns what octave an rtMidi int code falls into, 0-11 being octave 0, counting up from there
    static const int GetOctaveFromIntCode(int code)

    ///Returns the midicode given a note and an octave
    static const int GetMidiCodeFromNoteAndOctave(const Note note, const int octave)

    ///Convenience method for getting the string representation of the Note enum
    static const std::string GetNoteName(const Note note)

    ///Query to get the amount of devices connected
    static const int GetNumberOfConnectedDevices()
