#pragma once

#include <cstdlib>
#include <signal.h>
#include <memory>	
#include <functional>
#include <map>
#include <set>
#include <ctime>

#include "RtMidi\RtMidi.h"
#include "MidiDefinitions.h"

/*Primary class for querying midi input data for a connected midi input device 
Is a wrapper around RtMidi. For a quick reference, as a base, RtMidi provides 2 bytes in a message. Byte 1 is the note, and Byte 2 is the pressure, (0 is released.)
No clue what byte 0 or stamp is, as of yet, sample code : 
	stamp = midiin->getMessage(&message);
	nBytes = message.size();
	for (i = 0; i<nBytes; i++)
		std::cout << "Byte " << i << " = " << (int)message[i] << ", ";
	if (nBytes > 0)
		std::cout << "stamp = " << stamp << std::endl;

	sample output for middle c up then down : 
	Byte 0 = 144, Byte 1 = 60, Byte 2 = 69, stamp = 0.024
	Byte 0 = 144, Byte 1 = 60, Byte 2 = 0, stamp = 0.011
*/
namespace MidiBop {
	class MidiDevice
	{
	public:	
		MidiDevice();	///Creates a new midi-keyboard interface.
		~MidiDevice(); ///Destructor for cleanup

		bool Initialise(unsigned int port = 0); ///Initialises the keyboard on certain port. defaults to port 0, but provides an argument to set port manually. Returns false if cannot find device
		bool IsDeviceConnected(); ///Returns true if a midi device on this port is connected. If this is fale, you might want to call initialise again.
		std::string GetDeviceName(); ///Returns the device name of this MidiDevice.
		void Update(); ///Runs an update tick on this keyboard object. Getting the rtMidi messages that are pending, and converting them to useful data structures

		void RegisterMidiCodeRecievedCallback(int code, std::function<void()> codeRecievedCallback); ///Register a callback that is fired when a specific rtMidi code is recieved from the connected midi keyboard device.
		void RegisterNoteInOctavePushedCallback(MidiBop::Note note, int octave, std::function<void(int, int)> noteInOctavePushedCallback); ///Register a callback that is fired when a specific Note letter in a specific octave is pressed.
		void RegisterAnyKeyPressedCallback(std::function<void(int, int)> anyKeyPressedCallback); ///Register a callback fired when a midi input press is recieved. First argument is midi-code, second is intensity of press.
		void RegisterKeyIntensityChangedCallback(std::function<void(int, int)> intensityChangeCallback); ///Register a callback fired when a midi input press is recieved on a key that is already pressed, and is of differing intensity. First argument is midi-code, second is intensity of press.
		void RegisterKeyReleasedCallback(std::function<void(int)> keyReleasedCallback);	 ///Register a callback fired when a midi input release is recieved. Argument is midi code of released key.
		void RegisterDeviceConnectedCallback(std::function<void(const std::string&, const MidiDevice&)> deviceConnectedCallback); ///Register a callback fired when a device on the port this MidiDevice is mapped to is connected, and wasnt connected before. First argument is name of device connected, second argument is reference to this MidiDevice object, which might be handy
		void RegisterDeviceDisconnectedCallback(std::function<void(const std::string&, const MidiDevice&)> deviceConnectedCallback); ///Register a callback fired when the connected device is disconnected. First argument is name of device that was just disconnected, second argument is reference to this MidiDevice object, which might be handy

		void DeregisterMidiCodeRecievedCallback(int code, std::function<void()> codeRecievedCallback); ///Deregister a midi code recieved callback function. Will remove only the first callback that matches, so if their are duplicates, some will remain.
		void DeregisterNoteInOctavePushedCallback(MidiBop::Note note, int octave, std::function<void(int, int)> noteInOctavePushedCallback); ///Deregister a note in octave pushed callback function. Will remove only the first callback that matches, so if their are duplicates, some will remain.
		void DeregisterAnyKeyPressedCallback(std::function<void(int, int)> keyPressedCallback); ///Deregister a keypressed callback function. Will remove only the first callback that matches, so if their are duplicates, some will remain.
		void DeregisterKeyIntensityChangedCallback(std::function<void(int, int)> intensityChangeCallback); ///Deregister an intensity changed callback function. Will remove only the first callback that matches, so if their are duplicates, some will remain.
		void DeregisterKeyReleasedCallback(std::function<void(int)> keyReleasedCallback); ///Deregister a key released callback function. Will remove only the first callback that matches, so if their are duplicates, some will remain.
		void DeregisterDeviceConnectedCallback(std::function<void(const std::string&, const MidiDevice&)> deviceConnectedCallback);  ///Deregister a device connected callback function. Will remove only the first callback that matches, so if their are duplicates, some will remain.
		void DeregisterDeviceDisconnectedCallback(std::function<void(const std::string&, const MidiDevice&)> deviceConnectedCallback);  ///Deregister a device connected callback function. Will remove only the first callback that matches, so if their are duplicates, some will remain.

		void DeregisterAllMidiCodeRecievedCallback(); ///Register a callback that is fired when a specific rtMidi code is recieved from the connected midi keyboard device.
		void DeregisterAllNoteInOctavePushedCallback(); ///Register a callback that is fired when a specific Note letter in a specific octave is pressed. Will not register duplicate functions. 
		void DeregisterAllAnyKeyPressedCallbacks(); ///Deregister all keypress callbacks that may be registered
		void DeregisterAllKeyIntensityChangedCallbacks(); ///Deregister all keyintensitychanged callbacks that may be registered
		void DeregisterAllKeyReleasedCallbacks(); ///Deregister all keyrelease callbacks that may be registered
		void DeregisterAllDeviceConnectedCallbacks(); ///Deregister all device connected callbacks that may be registers
		void DeregisterAllDeviceDisconnectedCallbacks(); ///Deregister all device disconnected callbacks that may be registered

		void DeregisterAllCallbacks(); ///Deregister absolutely every callback thats registered to this object

		bool KeyPressed(const int midiCode); ///Is the key bound to midiCode currenty pressed
		bool KeyPressed(const Note note, const int octave); ///Is the letter note in specified octave currently pressed	
		bool KeysPressed(const std::vector<int> midiCodes); ///Are all the keys bound to the midicodes in the passed in vector pressed all at once, good for chords.

		int GetKeyPressure(const int midiCode); ///If the key represented by midicode is currently pressed, return with what pressure it's pressed. Will be 0 if it is unpressed
		int GetKeyPressure(const Note note, const int octave);  ///If key of note in specified octave is currently pressed, return with what pressure it's pressed. Will be 0 if it is unpressed

		double GetTimeStampOfLastKeyEvent(const int midiCode); ///Gets the processor timestamp of the last midievent concerning this key
		double GetTimeStampOfLastKeyEvent(const Note note, const int octave); ///Gets the processor timestamp of the last midievent concerning this key

		std::map<int, std::pair<int,double>> GetKeys() const; ///Get a map containing the current state of the entire midi-board. Key is the midicode, value is a pair of the intensity of push, and the processor timestamp the last event about this key was registered. Intensities of 0 mean unpushed.
		std::map<int, std::pair<int, double>> GetPressedKeys() const; ///Get a map containing only the keys that are currently down. Key of the map is the midicode, value is a pair of intensity of push, and the processor timestamp of the last event about this key was registered.

		void EnableAutomaticConnection(); ///Enables the automatic connection attempts in Update, enabled by default
		void DisableAutomaticConnection(); ///Disables the automatic connection attempts in Update
		bool IsAutomaticConnectionEnabled(); ///Whether or not this MidiDevice will attempt to automatically connect in Update

		RtMidiIn* GetRtMidiIn(); ///Return the RtMidiIn object that drives this MidiDevice, if you want to interface with rtMidi directly.

	private:
		std::unique_ptr<RtMidiIn> midiin; ///The RtMidi input object, initialised in constructor

		void QueryMessageQueue(double &stamp, std::vector<unsigned char> &message) const; ///Grabs all the bytes from the message queue, as well as the stamp
		void ConvertMessageToMidiCodeAndIntensity(const std::vector<unsigned char> message, int &eventCode, int &midiCode, int &intensity) const; ///Converts the passed in message into the midi int code by unpacking it. Will return -1 if there isnt the bytes for the keypress. Also pass in a reference int to get the pressure of any pushed keys, if they're pushed at all. The event code is the first 4 bits of the first byte returned from RTMIDI, and is used to make sure we're recieving key pushed and not random shit.
		void FireCallbacks(const int eventCode, const int midiCode, const int intensity); ///Derive what keys were just pushed/released, and fire any bound callback events
		void UpdateCurrentKeyboardState(const int eventCode, const int midiCode, const int intensity); ///Update the pressed keys map with keys just pressed/released

		static void OnMidiError(RtMidiError::Type type, const std::string &errorText, void *userData);

		std::map<int, std::function<void()>> midiCodeRecievedCallbackFuncs; ///Callbacks that can be registered externally	Will fire when the midicode represented by the map key has been recieved.
		std::map<std::pair<MidiBop::Note, int>, std::function<void(int, int)>> specificNoteInOctavePushedCallbackFuncs; ///Callbacks that can be registered externally, will fire when a specific note in a specific octave has been pushed, will pass back the pressure the key was pushed with.
		std::vector<std::function<void(int, int)>> keyPressedCallbackFuncs; ///Callbacks that can be registered externally, will fire when a key is pressed, ergo when an event is recieved relating to a key that is not of pressure 0, when that key pressure was 0 previously
		std::vector<std::function<void(int, int)>> intensityChangedCallbackFuncs;	///Callbacks that can be registered externally, will fire when the intensity or pressure of a key is registered as differnt as the one previously stored, and the key is already pressed
		std::vector<std::function<void(int)>> keyReleasedCallbackFuncs; ///Callbacks that can be registered externally, will fire when an event of intensity 0 is recieved about a key that was previously above 0 intensity
		std::vector<std::function<void(std::string, const MidiDevice&)>> deviceConnectedCallbackFuncs; ///Callbacks that can be registered externally. Will fire when a device that wasn't previously connected is connected. Arguments are the name of the device in question, and a reference to this MidiDevice object
		std::vector<std::function<void(std::string, const MidiDevice&)>> deviceDisconnectedCallbackFuncs; ///Callbacks that can be registered externally. Will fire when a device that was connected is disconnected. Arguments are the name of the device in question, and a reference to this MidiDevice object


		bool wasConnectedLastCheck; ///To faciliate firing device connected callbacks only once
		std::string lastConnectedDeviceName; ///The name of the midi device last connected, to facilitate sending the name of the device just disconnected
		unsigned int port; ///Set in initialise, the port this keyboard is on

		bool automaticConnect; ///Whether this object should attempt to reconnect automatically in update if it's not connected
		double lastStamp; ///The last stamp popped from the rtMidi queue
		std::vector<unsigned char> lastMessage; ///The last message popped from the rtMidi queue

		std::map<int, std::pair<int, double>> keys; ///The current state of the keys (or whatever notes are on-device), with the key being the midi key-code (60 = middle C as I recall,) and then a pair of the the pressure, 0 means unpressed, and the processor timestamp that an event about this key was last registered
		std::map<int, std::pair<int, double>> pressedKeys; ///Keep an active check on what keys are actually pressed. You can derive this from just the keys array. But this allows quick, inexpensive access, as the cost of a little constant overhead.
		static const int OCTAVE_RANGE = 10; ///How many octaves to support, an octave is a C-C range of 12 notes. Octave 0 is the lowest, with for example midi code 3 being D# in octave 0, whereas 15 is D# in octave 1 

		///Refer to this table : http://www.midimountain.com/midi/midi_status.htm
		///We only give a damn about key down, key up, and polyphonic aftertouch events
		static const int MIDI_CODES_TO_CARE_ABOUT_LOWER = 128;
		static const int MIDI_CODES_TO_CARE_ABOUT_UPPER = 175;

		///Filter most messages by this event code, so we only react to key presses.
		static const int KEY_PRESS_EVENT_CODE = 144;

		///Each update loop will process this number of midi events in the buffer. If you're calling update at any real sort of rate, (more than 30 per second would be my advice,) you should probably just leave this at one.
		static const int NUM_OF_MIDI_EVENTS_PER_FRAME = 1;
	};
}
