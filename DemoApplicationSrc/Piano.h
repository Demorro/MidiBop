#pragma once
#include <vector>
#include <SFML\Graphics.hpp>
#include "MidiDefinitions.h"
#include "MidiDevice.h"

///Representation of a piano. Draws keys and responds to midi input. Intended to test the Midibop library.
class Piano
{
public:
	Piano(int xPos, int yPos, MidiBop::MidiDevice *midiBopDevice, sf::Font* font);
	~Piano();

	void Update(); ///Main piano update loop
	void Render(sf::RenderWindow &renderWindow); ///Renders the keys of this piano, as stored as sprites in the pianoKeys map.

private:

	const sf::Color BLACK_KEY_COLOR; ///The colour of the black keys, tinted. Probably black
	const sf::Color WHITE_KEY_COLOR; ///The colour of the white keys, tinted. Probably white

	MidiBop::MidiDevice *midiDevice; ///Injected ptr to the MidiBop MidiDevice that drives this piano object. Passed in constructor 
	void CreatePianoKeys(); ///Creates the piano key sprites, loading them into the pianoKeys map and setting the correct note keys/positions
	void HighlightPressedKeys(); ///Run each update, highlight in green the keys that are currently pressed
	void ShowPressedKeyLabels(sf::RenderWindow &renderWindow); ///For each pressed key, show info on the midi state of that press.

	bool IsMidiIndexBlackKey(const int index) const; ///Returns true if the passed in midi index represents a black key

	sf::Texture whiteKeyTex; ///Texture for the white, non sharp keys of the piano.
	sf::Texture blackKeyTex; ///Texture for the black, sharp keys of the pian
	std::map<int, sf::Sprite> pianoKeys; ///Keys on the piano, paired as midi-note codes and sprites. Positioning/whether they're white or black is derived in constructor from piano layout
	static const int NO_OF_OCTAVES = 14; ///How many octaves this piano supports

	sf::Font *labelFont; ///Injected in, what font the pushnote labels are displayed in
	std::vector<sf::Text> keyNoteTexts; ///Labels for what notes are pushed displayed above the keys
	std::vector<sf::Text> intensityTexts; ///Labels for the intensity of the notes pushed,  displayed above the keys
	std::vector<sf::Text> midiCodeTexts; ///Labels for the midi codes of the notes pushed, displayed above the keys
	static const int LABEL_FONT_SIZE = 24; ///How big the labels for the note pushed info is.
	static const int KEY_NOTE_LABEL_Y = 160; ///The y position of the key note labels, so they can be positioned above the key. X is just the key position
	static const int INTENSITY_NOTE_LABEL_Y = 125; ///The y position of the key intensity labels, so they can be positioned above the key. X is just the key position
	static const int MIDI_CODE_LABEL_Y = 100; ///The y position of the midi code labels, so they can be positioned above the key. X is just the key position

	sf::Vector2i pianoPos; ///The position this piano is drawn at, passed in from constructor
};

