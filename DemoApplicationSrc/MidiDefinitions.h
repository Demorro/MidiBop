#pragma once
#include <string>
#include <iostream>
#include <memory>
#include "RtMidi\RtMidi.h"

///File for definations that make handling midi messages easier. For notes and pressures and whatnot

namespace MidiBop {
	///Musical Notes
	enum Note
	{
		C,
		CSHARP,
		D,
		DSHARP,
		E,
		F,
		FSHARP,
		G,
		GSHARP,
		A,
		ASHARP,
		B
	};
	///Counts up through the notes octave by octave, and returns what note an int code represents
	static const Note GetNoteFromIntCode(const int code)
	{
		Note note = (Note)(code % 12);
		return note;
		
	}
	///Returns what octave an rtMidi int code represents, 0-11 being octave 0, counting up from there
	static const int GetOctaveFromIntCode(int code)
	{
		int octave = 0;
		while (code > 12)
		{
			code -= 12;
			octave++;
		}
		return octave;
	}
	///Returns the midicode given a note and an octave
	static const int GetMidiCodeFromNoteAndOctave(const Note note, const int octave)
	{
		int code = 0;
		code += (octave * 12);
		code += note;
		return code;
	}
	///Convenience method for getting the string representation of the Note enum
	static const std::string GetNoteName(const Note note)
	{
		switch (note)
		{
		case A:
			return "A";
		case ASHARP:
			return "A#";
		case B:
			return "B";
		case C:
			return "C";
		case CSHARP:
			return "C#";
		case D:
			return "D";
		case DSHARP:
			return "D#";
		case E:
			return "E";
		case F:
			return "F";
		case FSHARP:
			return "F#";
		case G:
			return "G";
		case GSHARP:
			return "G#";
		default:
			std::cout << "Couldn't find note" << std::endl;
			return "NONOTE";
			break;
		}
	}

	///Query to get the amount of devices connected
	static const int GetNumberOfConnectedDevices()
	{
		auto midiIn = std::make_unique<RtMidiIn>();
		return midiIn->getPortCount();
	}
}