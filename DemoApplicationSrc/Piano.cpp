#include "Piano.h"
#include "Assets.h"
#include <algorithm>


Piano::Piano(int xPos, int yPos, MidiBop::MidiDevice *midiBopDevice, sf::Font* font) : WHITE_KEY_COLOR(sf::Color::White), BLACK_KEY_COLOR(50,50,50,255)
{
	this->midiDevice = nullptr;
	this->midiDevice = midiBopDevice;

	//Load the piano key textures
	whiteKeyTex.loadFromFile(WHITE_KEY_PATH);
	blackKeyTex.loadFromFile(BLACK_KEY_PATH);

	pianoPos = sf::Vector2i(xPos, yPos);
	labelFont = font;

	CreatePianoKeys();
}

Piano::~Piano()
{
	
}

void Piano::Update()
{
	HighlightPressedKeys();
}

void Piano::Render(sf::RenderWindow &renderWindow)
{
	for (int i = 0; i < NO_OF_OCTAVES * 12; i++)
	{
		if (!IsMidiIndexBlackKey(i))
		{
			renderWindow.draw(pianoKeys[i]);
		}
	}
	//render black keys on top of white ones
	for (int i = 0; i < NO_OF_OCTAVES * 12; i++)
	{
		if (IsMidiIndexBlackKey(i))
		{
			renderWindow.draw(pianoKeys[i]);
		}
	}

	ShowPressedKeyLabels(renderWindow);
}


void Piano::CreatePianoKeys()
{
	pianoKeys.clear();

	int currentXPlacementPos = pianoPos.x; ///Increment the xPos each loop round by the appropriate key seperation

	for (int i = 0; i < NO_OF_OCTAVES * 12; i++)
	{

		if (IsMidiIndexBlackKey(i)) { pianoKeys.insert(std::pair<int, sf::Sprite>(i, sf::Sprite(blackKeyTex))); }
		else { pianoKeys.insert(std::pair<int, sf::Sprite>(i, sf::Sprite(whiteKeyTex))); }
		
		pianoKeys[i].setPosition(currentXPlacementPos, pianoPos.y);
		

		if ((i%12 == 4) || (i%12 == 11)) //4 and 11 are E->F and B->C, the keys without black gaps
		{
			currentXPlacementPos += whiteKeyTex.getSize().x;
		}
		else
		{
			currentXPlacementPos += (whiteKeyTex.getSize().x / 2);
		}

	

		if (IsMidiIndexBlackKey(i))
		{
			pianoKeys[i].setColor(sf::Color::Black);
			pianoKeys[i].move(blackKeyTex.getSize().x / 2, 0);
		}

		keyNoteTexts.push_back(sf::Text("A", *labelFont, LABEL_FONT_SIZE));
		keyNoteTexts[keyNoteTexts.size() - 1].setPosition(pianoKeys[i].getPosition().x + (pianoKeys[i].getLocalBounds().width/2) - keyNoteTexts[keyNoteTexts.size() - 1].getLocalBounds().width/2, KEY_NOTE_LABEL_Y);		
		keyNoteTexts[keyNoteTexts.size() - 1].setColor(sf::Color::Green);

		intensityTexts.push_back(sf::Text("AA", *labelFont, LABEL_FONT_SIZE));
		intensityTexts[intensityTexts.size() - 1].setPosition(pianoKeys[i].getPosition().x + (pianoKeys[i].getLocalBounds().width / 2) - intensityTexts[intensityTexts.size() - 1].getLocalBounds().width / 2, INTENSITY_NOTE_LABEL_Y);
		intensityTexts[intensityTexts.size() - 1].setColor(sf::Color::Red);

		midiCodeTexts.push_back(sf::Text("AA", *labelFont, LABEL_FONT_SIZE));
		midiCodeTexts[midiCodeTexts.size() - 1].setPosition(pianoKeys[i].getPosition().x + (pianoKeys[i].getLocalBounds().width / 2) - midiCodeTexts[midiCodeTexts.size() - 1].getLocalBounds().width / 2, MIDI_CODE_LABEL_Y);
		midiCodeTexts[midiCodeTexts.size() - 1].setColor(sf::Color::Cyan);
	}
}

void Piano::HighlightPressedKeys()
{
	if (midiDevice == nullptr) {
		return;
	}

	//Set all keys unhighlighted
	for (int i = 0; i < pianoKeys.size(); i++)
	{
		if (IsMidiIndexBlackKey(i)) { pianoKeys[i].setColor(BLACK_KEY_COLOR); }
		else { pianoKeys[i].setColor(WHITE_KEY_COLOR); }
	}

	for (auto key : midiDevice->GetPressedKeys())
	{
		if (pianoKeys.count(key.first) >= 1)
		{
			int intensity = 3 * key.second.first; //key.second.first is key hit intensity, the multiplication is just to make it easier to see
			if (intensity > 255) {
				intensity = 255;
			}
			pianoKeys[key.first].setColor(sf::Color(0, intensity, 0, 255));
		}
	}

}

void Piano::ShowPressedKeyLabels(sf::RenderWindow &renderWindow)
{
	for (int i = 0; i < keyNoteTexts.size(); i++)
	{
		if(midiDevice->GetPressedKeys().count(i) >= 1)
		{
			keyNoteTexts[i].setString(MidiBop::GetNoteName(MidiBop::GetNoteFromIntCode(i)));
			renderWindow.draw(keyNoteTexts[i]);

			intensityTexts[i].setString(std::to_string(midiDevice->GetPressedKeys()[i].first));
			renderWindow.draw(intensityTexts[i]);

			midiCodeTexts[i].setString(std::to_string(i));
			renderWindow.draw(midiCodeTexts[i]);
		}
	}
}

bool Piano::IsMidiIndexBlackKey(int index) const {
	int blackKeysIndex = index % 12;
	if ((blackKeysIndex == 1) || (blackKeysIndex == 3) || (blackKeysIndex == 6) || (blackKeysIndex == 8) || (blackKeysIndex == 10))
	{
		return true;
	}
	return false;
}
