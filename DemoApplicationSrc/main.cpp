#include <iostream>
#include <SFML/Graphics.hpp>
#include "MidiDefinitions.h"
#include "MidiDevice.h"
#include "Piano.h"
#include <memory>
#include "Assets.h"

using namespace MidiBop;

bool done;
static void finish(int ignore) { done = true; }		  

sf::Text connectedDeviceText;

void DeviceConnectedCallback(const std::string &deviceName, const MidiBop::MidiDevice &device)
{
	connectedDeviceText.setString(deviceName);
}
void DeviceDisconnectedCallback(const std::string &deviceName, const MidiBop::MidiDevice &device)
{
	connectedDeviceText.setString("No Device Connected. Last Device : " + deviceName);
}

int main()
{
	sf::RenderWindow window(sf::VideoMode(1400, 400), "MidiBop Demo");
	
	sf::Font font;
	font.loadFromFile(FONT_PATH);

	sf::Texture keyKeyTexture;
	keyKeyTexture.loadFromFile(KEY_KEY_PATH);
	sf::Sprite keyKeySprite;
	keyKeySprite.setTexture(keyKeyTexture);
	keyKeySprite.setPosition(600, -20);

	std::unique_ptr<MidiDevice> midiDevice = std::make_unique<MidiDevice>();
	midiDevice->Initialise();
	midiDevice->RegisterDeviceConnectedCallback(DeviceConnectedCallback);
	midiDevice->RegisterDeviceDisconnectedCallback(DeviceDisconnectedCallback);
	std::unique_ptr<Piano> piano = std::make_unique<Piano>(-840,210, midiDevice.get(), &font);

	
	connectedDeviceText.setFont(font);
	connectedDeviceText.setColor(sf::Color::White);
	connectedDeviceText.setCharacterSize(32);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		midiDevice->Update();
		piano->Update();

		window.draw(keyKeySprite);
		piano->Render(window);
		window.draw(connectedDeviceText);
		window.display();
	}

	return 0;
}