#include "MidiDevice.h"
#include "MidiDefinitions.h"
namespace MidiBop {
	MidiDevice::MidiDevice()
	{
		automaticConnect = true;
		wasConnectedLastCheck = false;
		port = 0U;

		//Preload all the keys in the keyspressed map
		for (int i = 0; i < OCTAVE_RANGE * 12; i++)
		{
			keys.insert(std::pair<int, std::pair<int, double>>(i, std::pair<int, double>(0, 0)));
		}

		midiin = nullptr;
	}
	MidiDevice::~MidiDevice()
	{
		if (midiin != nullptr)
		{
			if (midiin->isPortOpen()) {
				midiin->closePort();
			}
			midiin.release();
		}
	}

	bool MidiDevice::Initialise(unsigned int port)
	{

		if (midiin == nullptr) {
			midiin = std::make_unique<RtMidiIn>();
		}


		if (midiin->getPortCount() > 0)
		{
			if (midiin->isPortOpen() == false)
			{
				std::string portName = midiin->getPortName(port);
				if (portName == "") {
					midiin.reset();
					midiin = nullptr;
					return false;
				}
			}
		}
		else
		{
			midiin.reset();
			midiin = nullptr;
			return false;
		}

		midiin->setErrorCallback(MidiDevice::OnMidiError);
		midiin->openPort(port); //Open the port as passed in from port argument
		midiin->ignoreTypes(false, false, false); //Don't ignore sysex, timing, or active sensing messages.

		this->port = port;
		return true;
	}
	void MidiDevice::Update()
	{
		if ((automaticConnect) && (IsDeviceConnected() == false)) {
			Initialise(port);
		}

		int pushIntensity = 0;
		int midiCode = -1;
		int eventCode = -1;

		if (midiin == nullptr) {
			FireCallbacks(eventCode, midiCode, pushIntensity);
			return;
		}

		for (int i = 0; i < NUM_OF_MIDI_EVENTS_PER_FRAME; i++)
		{
			QueryMessageQueue(lastStamp, lastMessage);
			ConvertMessageToMidiCodeAndIntensity(lastMessage, eventCode, midiCode, pushIntensity);
			FireCallbacks(eventCode, midiCode, pushIntensity);
			UpdateCurrentKeyboardState(eventCode, midiCode, pushIntensity);
		}
	}

	void MidiDevice::QueryMessageQueue(double &stamp, std::vector<unsigned char> &message) const
	{
		stamp = midiin->getMessage(&message);
	}
	void MidiDevice::ConvertMessageToMidiCodeAndIntensity(const std::vector<unsigned char> message, int &eventCode, int &midiCode, int &intensity) const
	{
		int nBytes = message.size();
		eventCode = -1;
		midiCode = -1;
		intensity = 0;

		if (nBytes > 0) {eventCode = ((int)message[0] & 0xF0);}		
		if (nBytes > 1) { midiCode = ((int)message[1]); }
		if (nBytes > 2) { intensity = ((int)message[2]); }
	}
	void MidiDevice::FireCallbacks(const int eventCode, const int midiCode, const int intensity)
	{

		//Fire device connected events
		if ((wasConnectedLastCheck == false) && (IsDeviceConnected()))
		{
			wasConnectedLastCheck = true;
			for (auto &connectedCallback : deviceConnectedCallbackFuncs)
			{
				if (connectedCallback != nullptr)
				{
					connectedCallback(GetDeviceName(), *this);
				}
			}
			lastConnectedDeviceName = GetDeviceName();
		}

		//Fire device disconnected events
		if ((wasConnectedLastCheck == true) && (!IsDeviceConnected()))
		{
			wasConnectedLastCheck = false;
			for (auto &disconnectedCallback : deviceDisconnectedCallbackFuncs)
			{
				if (disconnectedCallback != nullptr)
				{
					disconnectedCallback(lastConnectedDeviceName, *this);
				}
			}
		}

		//Fire recieved midicode events
		if (midiCodeRecievedCallbackFuncs.count(midiCode) > 1)
		{
			midiCodeRecievedCallbackFuncs[midiCode]();
		}


		//Only care about key up/down events after this point
		if (eventCode != KEY_PRESS_EVENT_CODE) {
			return;
		}

		//Fire specific key pressed events
		if (specificNoteInOctavePushedCallbackFuncs.count(std::pair<MidiBop::Note, int>(MidiBop::GetNoteFromIntCode(midiCode), MidiBop::GetOctaveFromIntCode(midiCode))) > 0)
		{
			specificNoteInOctavePushedCallbackFuncs[std::pair<MidiBop::Note, int>(MidiBop::GetNoteFromIntCode(midiCode), MidiBop::GetOctaveFromIntCode(midiCode))](midiCode, intensity);
		}

		//Fire any key pressed events
		if ((intensity > 0) && (keys[midiCode].first <= 0))
		{
			for (auto &keyPressCallback : keyPressedCallbackFuncs)
			{
				if (keyPressCallback != nullptr)
				{
					keyPressCallback(midiCode, intensity);
				}
			}
		}
		//Fire pressure changed events
		else if ((intensity > 0) && (keys[midiCode].first > 0))
		{
			for (auto &intensityChangedCallback : intensityChangedCallbackFuncs)
			{
				if (intensityChangedCallback != nullptr)
				{
					intensityChangedCallback(midiCode, intensity);
				}
			}
		}
		//Fire key released events
		else if ((intensity <= 0) && (keys[midiCode].first > 0))
		{
			for (auto &keyReleasedCallback : keyReleasedCallbackFuncs)
			{
				if (keyReleasedCallback != nullptr)
				{
					keyReleasedCallback(midiCode);
				}
			}
		}

		
	}
	void MidiDevice::UpdateCurrentKeyboardState(const int eventCode, const int midiCode, const int intensity)
	{
		if (eventCode != KEY_PRESS_EVENT_CODE) {
			return;
		}

		if ((midiCode >= 0) && (midiCode < (OCTAVE_RANGE * 12)))
		{
			keys[midiCode].first = intensity;
			keys[midiCode].second = double(clock());

			//Handle putting stuff in/out of the pressedkeys map
			//If the intensity is greater than 0, then we want this key to be in the pushed keys map
			if (intensity > 0)
			{
				if (pressedKeys.count(midiCode) <= 0) { pressedKeys.insert(std::pair<int, std::pair<int, double>>(midiCode, std::pair<int, double>(intensity, double(clock())))); }
			}
			//But if the intensity is 0, then if it is in the pushed keys map, we want to remove it
			else if (intensity <= 0)
			{
				if (pressedKeys.count(midiCode) > 0) { pressedKeys.erase(midiCode); }
			}
		}
	}

	bool MidiDevice::IsDeviceConnected()
	{
		if (midiin == nullptr)
		{
			return false;
		}

		std::string portName = midiin->getPortName(port);
		if (portName == "") {
			midiin = nullptr;
			return false;
		}

		return true;
	}
	std::string MidiDevice::GetDeviceName() {
		if (IsDeviceConnected())
		{
			return midiin->getPortName(port);
		}
		return "";
	}
	void MidiDevice::EnableAutomaticConnection()
	{
		automaticConnect = true;
	}
	void MidiDevice::DisableAutomaticConnection()
	{
		automaticConnect = false;
	}
	bool MidiDevice::IsAutomaticConnectionEnabled()
	{
		return automaticConnect;
	}

	void MidiDevice::RegisterMidiCodeRecievedCallback(int code, std::function<void()> codeRecievedCallback)
	{
		midiCodeRecievedCallbackFuncs.insert(std::pair<int, std::function<void()>>(code, codeRecievedCallback));
	}
	void MidiDevice::RegisterNoteInOctavePushedCallback(MidiBop::Note note, int octave, std::function<void(int, int)> noteInOctavePushedCallback)
	{
		specificNoteInOctavePushedCallbackFuncs.insert(std::pair<std::pair<MidiBop::Note, int>, std::function<void(int, int)>>(std::pair<MidiBop::Note, int>(note, octave), noteInOctavePushedCallback));
	}
	void MidiDevice::RegisterAnyKeyPressedCallback(std::function<void(int, int)> anyKeyPressedCallback)
	{
		keyPressedCallbackFuncs.push_back(anyKeyPressedCallback);
	}
	void MidiDevice::RegisterKeyIntensityChangedCallback(std::function<void(int, int)> intensityChangeCallback)
	{
		intensityChangedCallbackFuncs.push_back(intensityChangeCallback);
	}
	void MidiDevice::RegisterKeyReleasedCallback(std::function<void(int)> keyReleasedCallback)
	{
		keyReleasedCallbackFuncs.push_back(keyReleasedCallback);
	}
	void MidiDevice::RegisterDeviceConnectedCallback(std::function<void(const std::string&, const MidiDevice&)> deviceConnectedCallback)
	{
		deviceConnectedCallbackFuncs.push_back(deviceConnectedCallback);
	}
	void MidiDevice::RegisterDeviceDisconnectedCallback(std::function<void(const std::string&, const MidiDevice&)> deviceDisconnectedCallback)
	{
		deviceDisconnectedCallbackFuncs.push_back(deviceDisconnectedCallback);
	}

	void MidiDevice::DeregisterMidiCodeRecievedCallback(int code, std::function<void()> codeRecievedCallback)
	{  
		if (midiCodeRecievedCallbackFuncs.count(code) > 0) { midiCodeRecievedCallbackFuncs.erase(code); }
	}
	void MidiDevice::DeregisterNoteInOctavePushedCallback(MidiBop::Note note, int octave, std::function<void(int, int)> noteInOctavePushedCallback)
	{
		if (specificNoteInOctavePushedCallbackFuncs.count(std::pair<MidiBop::Note, int>(note, octave)) > 0) {
			specificNoteInOctavePushedCallbackFuncs.erase(std::pair<MidiBop::Note, int>(note, octave));
		}
	}
	void MidiDevice::DeregisterAnyKeyPressedCallback(std::function<void(int, int)> anyKeyPressedCallback)
	{
		for (size_t i = 0; i < keyPressedCallbackFuncs.size(); i++)
		{
			if (keyPressedCallbackFuncs[i].target<void(int, int)>() == anyKeyPressedCallback.target<void(int, int)>())
			{
				keyPressedCallbackFuncs.erase(keyPressedCallbackFuncs.begin() + i);
				return;
			}
		}
	}
	void MidiDevice::DeregisterKeyIntensityChangedCallback(std::function<void(int, int)> intensityChangeCallback)
	{
		for (size_t i = 0; i < intensityChangedCallbackFuncs.size(); i++)
		{
			if (intensityChangedCallbackFuncs[i].target<void(int, int)>() == intensityChangeCallback.target<void(int, int)>())
			{
				intensityChangedCallbackFuncs.erase(intensityChangedCallbackFuncs.begin() + i);
				return;
			}
		}
	}
	void MidiDevice::DeregisterKeyReleasedCallback(std::function<void(int)> keyReleasedCallback)
	{
		for (size_t i = 0; i < keyReleasedCallbackFuncs.size(); i++)
		{
			if (keyReleasedCallbackFuncs[i].target<void(int)>() == keyReleasedCallback.target<void(int)>())
			{
				keyReleasedCallbackFuncs.erase(keyReleasedCallbackFuncs.begin() + i);
				return;
			}
		}
	}
	void MidiDevice::DeregisterDeviceConnectedCallback(std::function<void(const std::string&, const MidiDevice&)> deviceConnectedCallback)
	{
		for (size_t i = 0; i < deviceConnectedCallbackFuncs.size(); i++)
		{
			if (deviceConnectedCallbackFuncs[i].target<void(std::string, const MidiDevice&)>() == deviceConnectedCallback.target<void(std::string, const MidiDevice&)>())
			{
				deviceConnectedCallbackFuncs.erase(deviceConnectedCallbackFuncs.begin() + i);
				return;
			}
		}
	}
	void MidiDevice::DeregisterDeviceDisconnectedCallback(std::function<void(const std::string&, const MidiDevice&)> deviceDisconnectedCallback)
	{
		for (size_t i = 0; i < deviceDisconnectedCallbackFuncs.size(); i++)
		{
			if (deviceDisconnectedCallbackFuncs[i].target<void(std::string, const MidiDevice&)>() == deviceDisconnectedCallback.target<void(std::string, const MidiDevice&)>())
			{
				deviceDisconnectedCallbackFuncs.erase(deviceDisconnectedCallbackFuncs.begin() + i);
				return;
			}
		}
	}

	void MidiDevice::DeregisterAllMidiCodeRecievedCallback()
	{
		midiCodeRecievedCallbackFuncs.clear();
	}
	void MidiDevice::DeregisterAllNoteInOctavePushedCallback()
	{
		specificNoteInOctavePushedCallbackFuncs.clear();
	}
	void MidiDevice::DeregisterAllAnyKeyPressedCallbacks()
	{
		keyPressedCallbackFuncs.clear();
	}
	void MidiDevice::DeregisterAllKeyIntensityChangedCallbacks()
	{
		intensityChangedCallbackFuncs.clear();
	}
	void MidiDevice::DeregisterAllKeyReleasedCallbacks()
	{
		keyReleasedCallbackFuncs.clear();
	}
	void MidiDevice::DeregisterAllDeviceConnectedCallbacks() 
	{
		deviceConnectedCallbackFuncs.clear();
	}
	void MidiDevice::DeregisterAllDeviceDisconnectedCallbacks()
	{
		deviceDisconnectedCallbackFuncs.clear();
	}

	void MidiDevice::DeregisterAllCallbacks()
	{
		DeregisterAllMidiCodeRecievedCallback();
		DeregisterAllNoteInOctavePushedCallback();
		DeregisterAllAnyKeyPressedCallbacks();
		DeregisterAllKeyIntensityChangedCallbacks();
		DeregisterAllKeyReleasedCallbacks();
		DeregisterAllDeviceConnectedCallbacks();
		DeregisterAllDeviceDisconnectedCallbacks();
	}

	bool MidiDevice::KeyPressed(const int midiCode)
	{
		if (keys.count(midiCode) >= 1) {
			if (keys[midiCode].first > 0)
			{
				return true;
			}
		}
		return false;
	}
	bool MidiDevice::KeyPressed(const Note note, const int octave)
	{
		int midiCode = GetMidiCodeFromNoteAndOctave(note, octave);
		return KeyPressed(midiCode);
	}
	bool MidiDevice::KeysPressed(const std::vector<int> midiCodes)
	{
		for (int code : midiCodes)
		{
			if (!KeyPressed(code))
			{
				return false;
			}
		}
		return true;
	}

	int MidiDevice::GetKeyPressure(const int midiCode)
	{
		if (KeyPressed(midiCode))
		{
			return keys[midiCode].first;
		}
		return 0;
	}
	int MidiDevice::GetKeyPressure(const Note note, const int octave)
	{
		int midiCode = GetMidiCodeFromNoteAndOctave(note, octave);
		return GetKeyPressure(midiCode);
	}

	double MidiDevice::GetTimeStampOfLastKeyEvent(const int midiCode)
	{
		if (keys.count(midiCode) >= 1) {
			return keys[midiCode].second;
		}
		return 0;
	}
	double MidiDevice::GetTimeStampOfLastKeyEvent(const Note note, const int octave)
	{
		return GetTimeStampOfLastKeyEvent(GetMidiCodeFromNoteAndOctave(note, octave));
	}

	std::map<int, std::pair<int, double>> MidiDevice::GetKeys() const
	{
		return keys;
	}
	std::map<int, std::pair<int, double>> MidiDevice::GetPressedKeys() const
	{
		return pressedKeys;
	}

	RtMidiIn* MidiDevice::GetRtMidiIn()
	{
		return midiin.get();
	}
	void MidiDevice::OnMidiError(RtMidiError::Type type, const std::string &errorText, void *userData)
	{
		std::cout << "Midi Error : " + errorText << std::endl;
	}

}

