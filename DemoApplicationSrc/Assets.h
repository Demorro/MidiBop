#pragma once
#include <string>

static const std::string WHITE_KEY_PATH = "..\\Assets\\WhiteKey.png";
static const std::string BLACK_KEY_PATH = "..\\Assets\\BlackKey.png";
static const std::string KEY_KEY_PATH = "..\\Assets\\KeyKey.png";
static const std::string FONT_PATH = "..\\Assets\\Montserrat-Bold.ttf";